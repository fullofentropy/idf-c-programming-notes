/***************************** getc and putc **************************************/

/*
getc()

int getc(FILE, *stream);

Purpose: Gets an unsigned char from a stream

Arguments: Stream Pointer

Return Value: This function returns the character read as an unsigned char cast to an int
or EOF on end of file
or 
error.

Syntax Example:

    int userInput = 0;                    // Will store input
    printf("Enter a character: ");        // Prompts user
    userInput = getc(stdin);              // Stores stream input
    NOTE* getchar() == getc(stdin) 

******************************************************************************************************************
putc()

int putc(int char, FILE *stream);

Purpose: Writes an unsigned char to a stream

Arguments:
    Integer value of character to write
    Stream pointer to write it to
Return Value: This function returns the character read as an unsigned char cast to an int 
or EOF on end of file 
or 
error.

Syntax Example:

printf("Your character was: ");        // Prefaces output
putc(userInput, stdout);               // Writes to stdout



 */