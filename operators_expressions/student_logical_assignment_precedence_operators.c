/*  
*************** Logical Operators *****************************


Logical operators test for a condition. Or more specifically, compares 
the binary state of one or more operands. Binary state is represented as 
True (1) or False (0).

 Main operations include:
    
    Conjunction (AND)  // &&
    Disjunction (OR)   // ||
    Negation (NOT      // !


Logical Operation	C Operator
_________________________________
      AND	           &&
      OR	           ||
      NOT	            !


______________________

Operator	Meaning	       Example	  Result: 1 (true) or 0 (false)
_____________________________________________________________________________________________
  &&	   Logical AND	   x && 1      if both x and y are != 0
  ||	   Logical OR	   x || y	   1 if either or both of x and y is != 0
  !	       Logical NOT	    !x	       1 if x == 0. In all other cases, the expression yields 0



        AND
   p    q    p&&q   // if one is false it's all false (false = 0) - both false is all false
   ______________
   1    1     1
   1    0     0
   0    1     0
   0    0     0


        OR
   p    q    p||q   //if only one is false it's all true (true = 1) -  both false is all false
   ______________
   1    1     1
   1    0     1
   0    1     1
   0    0     0  


        NOT
   p    q     !q    // if one is true the other is false
   ______________
   1    !    0
   0    !    1


EXAMPLES:
Operators	Example	  Result
________________________________
  &&	    1 && 0   	0
  ||	    1 || 0	    1
  !	         !1       	0
  &&	    0 && 9   	0
  ||	    0 || 9   	1
   !	     !0       	1
  &&	    9 && 1   	1
  ||	    9 || 1  	1
   !	      !9	    0


*************** Assignment Operators *****************************

Assignment operators include both simple and compound assignment.

Simple Assignment Operands:
    Two operands with arithmetic types
    Two operands with the same structure or union type*
    Two pointers that both point to the same type*

Compound Assignment
    Takes the form x op= y
    op is an arithmetic or bitwise* operator
    The value of x op (y) is assigned to x


Examples:
Operator	Meaning	             Example	  Result
______________________________________________________________________________-
  =	        Simple assignment	  x = y	      Assign the value of y to x
  op=	    Compound assignment	  x += y	  x op= y is equivalent to x = x op (y)


Example	   Result:
___________________
10 *= 2	     20
10 /= 2	      5
10 %= 2	      0
10 += 2	     12
10 -= 2	      8
10 += ++2	 13
10 += 2++	 12
10 -= --2	  9
10 -= 2--	  8


*************** Precedence/Order of Operations *****************************

Precedence determines the grouping of operands with operators in expressions with more than one operator. 
In other words, precedence is how and when a group of expressions is ran.

    Part of the precedence includes usual arithmetic rules regarding order of operations

        1 + 2 * 3            // Equivalent to 1 + (2 * 3)

    Use parentheses to manually control the grouping

         (1 + 2) * 3          // Result: 9


Priority	    Operator	                      GroupinG
___________________________________________________________
  1	         {} [] -> .	                      left to right
  2	    ! ~ ++ -- = - (type)* & sizeof	      right to left
  3	        * / %	                          left to right
  4	          + -	                          left to right
  5	        << >>	                          left to right
  6     	< <= > >=	                      left to right
  7	         == !=	                          left to right
  8	           &	                          left to right
  9	           ^	                          left to right
 10	           |	                          left to right
 11	          &&	                          left to right
 12	          ||	                          left to right
 13	          ?:	                          right to left
 14 	= += -= *= /= %= &= ^= != <<= >>=	  right to left
 15	           ,                              left to right



*/