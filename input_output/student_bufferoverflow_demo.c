#include <stdio.h>
#include <string.h>

/* Buffer Overflow
A buffer is a temporary area for data storage. When more data 
(than was originally allocated to be stored) gets placed by a program or system process, 
the extra data overflows. It causes some of that data to leak out into other buffers, 
which can corrupt or overwrite whatever data they were holding. */

int main(void)
{
    char password[15];
    int passed = 0;

    printf("Password:\n");
    // The gets() function does not check the array bounds and can even write strings of lengths greater
    // than the size of the buffer to which the string is written.
    gets(password);

    if(strcmp(password, "90cos"))
    {
        printf ("Wrong Password!\n");
    }
    else
    {
        printf ("Correct Password!\n");
        passed = 1;
    }

    if(passed)
    {
       /* Now Give root or admin rights to user*/
        printf ("Access Granted!");

        // an attacker can supply an input of length greater than what buffer can hold and at a particular 
        // length of input the buffer overflow overwrites the memory of integer ‘pass’. So despite a wrong password, 
        // the value of ‘pass’ can become non zero and hence root privileges can be granted to an attacker.
    }

    return 0;
}

/* To avoid buffer overflow attacks, the general advice that is given to programmers is to follow good programming practices. For example:
Make sure that the memory auditing is done properly in the program using utilities like "valgrind" memcheck(linux),
 use fgets() instead of gets(). Use strncmp() instead of strcmp(), strncpy() instead of strcpy() and so on.


How are buffer overflow errors are made?
These kinds of errors are very easy to make. For years they were a programmer’s nightmare. The problem lies in native C functions, which don’t care about doing appropriate buffer length checks. Below is the list of such functions and, if they exist, their safe equivalents:

    gets() -> fgets() - read characters
    strcpy() -> strncpy() - copy content of the buffer
    strcat() -> strncat() - buffer concatenation
    sprintf() -> snprintf() - fill buffer with data of different types
    (f)scanf() - read from STDIN
    getwd() - return working directory
    realpath() - return absolute (full) path

Use safe equivalent functions, which check the buffers length, whenever it’s possible. Namely:
    gets() -> fgets()
    strcpy() -> strncpy()
    strcat() -> strncat()
    sprintf() -> snprintf()
Functions that don’t have safe equivalents should be rewritten with safe checks implemented. Time spent on that will benefit in the future.
Remember that you have to do it only once.

Use compilers, which are able to identify unsafe functions, logic errors and check if the memory is overwritten when and where 
it shouldn’t be.

 */