#include <stdio.h>

int main()
{
    //////////////////////////////////
    ///// BASIC SCANF STATEMENTS /////
    //////////////////////////////////

    // int theAnswer;
    // float pi;
    // double posSqrtTwo;
    // char answerNum1;

    // printf("Enter an integer: \n");
    // scanf("%d", &theAnswer);            // Reads an integer from stdin

    // printf("Enter a float: \n");
    // scanf("%f", &pi);                   // Reads a float from stdin

    // printf("Enter a double: \n");
    // scanf("%lf", &posSqrtTwo);          // Reads a double from stdin

    // printf("Enter a character: \n"); //not that for a char you need to have a space before the %c or it reads a new line character
    // // scanf("%c", &answerNum1);           // Reads a char from stdin
    // scanf(" %c", &answerNum1);           // Reads a char from stdin


    // printf("%d \n", theAnswer);
    // printf("%f \n", pi);
    // printf("%lf \n", posSqrtTwo);
    // printf("%c \n", answerNum1);


    ///// BASIC SCANF STRING INPUT /////
    // char nickName[20] = {0};
    // printf("Enter a nickname \n");
    // scanf("%s", nickName);             // Dangerously reads string from stdin
    // printf("%s \n", nickName);
    // // scanf("%19s", nickName);           // Safer string read from stdin
    // // printf("%s \n", nickName);

   /*  conversion character initliazers demo 1  */

   ///// INITIALIZATION /////
// int num = 0;
// char letter = 0;

// printf("Enter a number then a letter: \n");
// ///// SCANF STATEMENT /////
// scanf("%d%c", &num, &letter);

// printf("You entered the number %d and the letter %c \n", num, letter);

// // %d%c  |  is the format
// // &num, &letter  |  are the arguments
// // &num == %d
// // &letter == %c


/* conversion character initliazers demo 2 */

 ////////// INITIALIZATION //////////
// int hours = 0;
// int minutes = 0;
// int seconds = 0;

// ////////// SCANF STATEMENTS //////////
// printf("Enter the hours, minutes and seconds: \n");
// //NOTE: The %*c conversion specification tells scanf to read char(s) in between integers but doesn't assign it to a variable.
// scanf("%d%*c%d%*c%d", &hours, &minutes, &seconds);

// printf("You entered: %d%d%d", hours, minutes, seconds);

// First %d == &hours
// Second %d == &minutes
// Third %d == &seconds

/* extra scanf() Syntax Examples: */

/////////////////////////////////////////
/////////// SCANF STATEMENTS ////////////
/////////////////////////////////////////


// scanf(“%d,%d”, &num1, &num2);    // Uses comma as delimiter
// scanf(“%3f”, &GPA);              // Only takes three inputs e.g., 3.4
// scanf(“*%lf”, &posSqrtTwo);      // Waits for asterisk before reading
// scanf(“%[abcd]c”, &ansNum1);     // Will only read an a, b, c, or d
// scanf(“%[A-Z]c”, &capsChar);     // Will only read a capital letter
// scanf(“%[A-z]c”, &rngOfChar);    // Only chars of decimal value 65–122
// scanf(“%32[01]s”, binaryStr);    // Stops reading at first non 0 or 1 
// scanf(“%64[^e\n]s”, Gadsby);     // Stops reading at first e or newline
// NOTE: Any field width specification also stops scanf() from reading any more input regardless of any matched characters.



 
    return 0;
}