/* 
BITWISE Operations 

A bit is the smallest unit of information on a computer. 
Bits are normally represented by 1 and 0. 

1 generally meaning [on, true, set, yes] 

and 

0 generally meaning [off, false, cleared, no]. 

Bitwise operators test and/or modify bits. 
We use bit manipulations to control the machine at the lowest level.


************************************ Numbering Systems ***************************************

DECIMAL              HEXADECIMAL             OCTAL                BINARY 
** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** 



Dec | 0  1   2   3   4   5    6     7    8     9    10     11   12    13    14   15     16     17  
HEX | 0  1   2   3   4   5    6     7    8     9    A      B    C     D     E     F     10     11
OCT | 0  1   2   3   4   5    6     7    10    11   12     13   14    15    16    17    20     21
BIN | 0  1  10  11  100  101  110  111  1000  1001  1010  1011 1100  1101  1110  1111  10000  10001







System	      Base    	Available Digits    Printf()   Format Example Value
____________________________________________________________________________
Decimal	       10	       [0-9]  	         %d         	42
Hexadecimal	   16	       0x[0-F]           %X	           0x2A
Octal	        8	       0[0-7]	         %o	           052
Binary	        2	       [0-1]	         %b	          101010


    Positional values are calculated by B^n where B = Base and n = digit position
    Calculation begins at the right and moves left
    42 == 0x2A == 052 == 101010
    The amount is the same, just counted differently
    Values are stored as binary despite original format

****
    How to:
    - Each digit is multiplied against its position value
    - Each product is added together to obtain the total 

   ____Digit____________________________________________ =  _____
   Positional Value:         B^3    B^2    B^1    B^0       total


See conversion pngs.
    conversion_dec_to_dec_and_hex_to_dec.png
    conversion_oct_to_dec_and_binary_to_dec.png
* * * * * * * * * * * * * * * * * * * * * * * * * * *


Hexadecimal for binary data:  Each Hex digit represents 4 binary bits

Hex -> 1      2    C    7    E    7
BIN -> 0001 0011 1100 0111 1110  0111







***





 */