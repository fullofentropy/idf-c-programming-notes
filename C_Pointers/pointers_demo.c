#include <stdio.h>

int main(){
    int var1 = 10;
    int var2 = 20;

    int *var_ptr = NULL;

    var_ptr = &var1;
    printf("var 2 = %d\n", var2);
    var2 = *var_ptr;

    printf("var1 = %d\n", var1);
    printf("var2 = %d\n", var2);
    printf("var_ptr = %p\n", var_ptr);
    printf("var1 address is: %p\n", &var1);
    printf("var2 address is: %p\n", &var2);

    int arr[10] = {0};

    for(int i = 0; i < 10; i++){
        printf("%p of index %i\n", &arr[i], i);
    }

    int *arr_ptr = &arr;
    printf("The array starts at %p\n", &arr);
    printf("The array starts at %p\n", arr_ptr);
}