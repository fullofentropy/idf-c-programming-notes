/* Formatting I/O

printf()

int printf(const char *format, expression-1, expression-2...);

Purpose: Writes output to stdout under control of the format string

Arguments: Output, format string

Return Value: Number of characters printed

Special:
    Conversion specifications translate printed variables
    Each conversion specifier is introduced by the % character
    You may include special formatting flags, set field width, and precision
    
    See conversion_characters.png


Precision (numbers):
%[flags][field width][.precision]specifier


float someNum = 12.3456;                    // Var used as comparison

printf("%f", someNum);                      // 12.345600
printf("%.0f", someNum);                    // 12
printf("%.2f", someNum);                    // 12.35
printf("%.3f", someNum);                    // 12.346
printf("%.6f", someNum);                    // 12.345600
printf("%.20f", someNum);                   // 12.34560012817382800000*


NOTE: The default precision for floating-point numbers is 6
* Your specific output may differ considering printf has exceeded 
the bounds of your float and is printing garbage
The special characters %d are called the integer conversion specification. 
When printf encounters a %d, it prints the value of the next expression in the 
list following the format string. This is called the parameter list.



 */