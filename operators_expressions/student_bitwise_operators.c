/* 
BITWISE OPERATIONS

- Bitwise Operations are performed on the entire binary representation of the value
- Bitwise Operations are performed on all bits separately


*** Note: Bitwise AND, OR, & NOT are similar to logical operators

** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** 
Bitwise AND:
The Bitwise AND operator will return 1 if a 1 is in both the x and y bit position; 0 otherwise.

    x & y         // 1 if 1 in both x and y


   x |  0  0  1  1  = 3
        &  &  &  &
   y |  0  1  1  0 = 6
   ____________________
 x&y |  0  0  1  0 = 2 

** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** 
Bitwise OR:
The bitwise OR operator will return 1 if 1 is in either x or y's given bit position; 0 otherwise.

    x | y         // 1 if 1 in either x or y, or both


   x |  0  0  1  1  = 3
        |  |  |  |
   y |  0  1  1  0 = 6
   ____________________
 x|y |  0  1  1  1 = 7 

    - Bitwise OR can be used to turn on some bits
    	
    -- BITWISE OR CONTINUED 
    uint16_t = bwResult = 0;                // 0000 0000 0000 0000
    uint16_t num1 = 12;                     // 0000 0000 0000 1100
    uint16_t num2 = 3;                      // 0000 0000 0000 0011
    bwResult = num1 | num2;                 // 0000 0000 0000 1111
    
    uint16_t num3 = 240;                    // 0000 0000 1111 0000
    bwResult = bwResult | num3;             // 0000 0000 1111 1111
    
    uint16_t num4 = 768;                    // 0000 0011 0000 0000
    bwResult = bwResult | num4;             // 0000 0011 1111 1111
    
    uint16_t turnOn = 65280;                // 1111 1111 0000 0000
    bwResult = bwResult | turnOn;           // 1111 1111 1111 1111



** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** 
Bitwise XOR:
The bitwise XOR operator will return 1 if 1 is in either x or y's bit position... but not both. Returns 0 otherwise.

    x ^ y         // 1, if 1 in either x or y, but not both

     x |  0  0  1  1  = 3
          ^  ^  ^  ^
     y |  0  1  1  0 = 6
   ____________________
   x^y |  0  1  0  1 = 5 


    --Bitwise XOR is often used as a means of doing a parity check. Assembly programmers also use XOR as 
    a shortcut to zeroing a register's value (num 1 ^ num 1 == 0). Other use cases for XOR are obfuscation* 
    and to swap variables without using a 'temp' variable.
    ***Do not confuse obfuscation with encryption. Obfuscation simply means to obscure something... to make it a bit harder to read or understand. **

   -- BITWISE XOR CONTiNUED
      Swapping values without a temp variable

        - SAMPLE EXECUTION OF BITWISE XOR SWAP
        uint8_t x = 1;                  // 0000 0001
        uint8_t y = 254;                // 1111 1110
        x = x ^ y;                      // x = 1111 1111 == (x)0000 0001 ^ (y)1111 1110
        y = x ^ y;                      // y = 0000 0001 == (x)1111 1111 ^ (y)1111 1110
        x = x ^ y;                      // x = 1111 1110 == 1111 1111 ^ 0000 0001
        
        - SAMPLE EXECUTION OF BITWISE XOR SWAP
        uint8_t p = 42;                 // 0010 1010
        uint8_t q = 11;                 // 0000 1011
        p = p ^ q;                      // p = 0010 0001 = (p)0010 1010 ^ (q)0000 1011
        q = p ^ q;                      // q = 0010 1010 = (p)0010 0001 ^ (q)0000 1011
        p = p ^ q;                      // p = 0000 1011 = (p)0010 0001 ^ (q)0010 1010




** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** 
Bitwise NOT:
The bitwise NOT operator changes bits that are 0 to 1 and 1 to 0.
--Note: to avoid negative numbers use 2 bytes as in the example below

    ~x            // 1, if 0 in x



    0000  0000  0000  0011  // unsigned short x=3
     ~     ~      ~    ~
    _______________________
    1111  1111  1111  1100  // ~x == 65532

    //////// CLEAR A FLAG  (value that acts as a signal for a function or process)////////
   - flagStorage stores 8 flags in a 32 bit int using hex digits 

    unsigned int flagStorage = 0x11111111;      // All flags are set
    unsigned int missingID = 0x00000010;        // Flat #2
    flagStorage &= ~missingID;                  // 0x11111101


    --//////// XOR WITH NOT ////////
        x ^ y == (~x & y) | (x & ~y)


** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** 
Shift Left:
he bitwise shift left operator shifts bits a certain amount of positions left. Bit positions 
vacated by the left shift are always filled with 0 bits. Bit values shifted out to the left are lost.


    x << y        // Each bit in x is shifted y positions left

    unsigned short x=3

              0000  0000  0000  0011
shift 6 left
              0000  0000  1100  0000
              _______________________
              x << 6 == 192



** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** ** 
Shift Right:
The bitwise shift right operator shifts bits a certain amount of positions right. 

Bit positions vacated by the right shift are filled with 0 bits if the left operand is unsigned or non-negative. 
If it's signed and negative, shift depends on the compiler. 
For instance, Visual Studio compiler is performing an arithmetic shift. 
In other words, the compiler is filling the left-most bits with the same bits as the sign which happens to be 1.

    x >> y        // Each bit in x is shifted y positions right


    unsigned shor x = 31337

               0111  1010  0110  1001
shift 6 right
               0000  0001  1110  1001
               ______________________
                x >> 6 == 489

 */