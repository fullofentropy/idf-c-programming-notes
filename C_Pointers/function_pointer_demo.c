#include <stdio.h>

void fun(int a)
{
    printf("Integer a is %d.\n", a);
}

int main()
{
    // Implicit
    void (*fun_ptr)(int) = fun;
    fun_ptr(10); 

    // Explicit
    void (*fun_ptr_ex)(int) = &fun;
    (*fun_ptr_ex)(10);

    printf("Function fun() address is: %p\n", fun_ptr);
    printf("Function fun() address is: %p\n", fun_ptr_ex);

}