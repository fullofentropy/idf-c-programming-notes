# Ubuntu

Some steps taken from: https://code.visualstudio.com/docs/cpp/config-linux

1. Download Ubuntu VM:
   - EDGE dev VM [image](https://developer.microsoft.com/en-us/microsoft-edge/tools/vms/) (~7 GB )  
   - full blow Dev VM [image](https://developer.microsoft.com/en-us/windows/downloads/virtual-machines/) (~20 GB) 
2. If using VirtualBox after adding VM:
   - increase VM display memory
   - mount VBoxGuestAdditions ISO (so display can auto scale)
   - run system updates (option, but suggested)
3. Follow guide from this link: https://code.visualstudio.com/docs/cpp/config-mingw
   1. Install the [C/C++ extension for VS Code](https://code.visualstudio.com/docs/cpp/config-mingw) 
   2. Install a c compiler. In this case will install the [mingw-64](https://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win32/Personal%20Builds/mingw-builds/installer/mingw-w64-install.exe/download). 
      - Run the MinGW-W64 installer
      - Select x86_x64 and click Next
      - copy the default install director and add to your windows path.