#include<stdio.h>
#include<string.h>
// https://90cos.gitlab.io/cyt/training/modules/C-Programming/Preprocessor/performance_labs/Lab25.html
// Make a program using preprocessor directives that:
//   1. Has a BUFFER_SIZE of 64
#define BUFFER_SIZE 64
//   2. Has a macro VICTORY that prints a victory message then exits the program
#define VICTORY()                                                                                  \
    {                                                                                              \
        printf("You typed the Magic Tacos Passcode\n");                                            \
        printf("returning zero\n");                                                                  \
        return 0;                                                                                  \
    }
// Then, in either main or a function:
//   1. Declare a string with a size of BUFFER_SIZE
//   2. Take user input as a string
//   3. If the user's string is "Tacos", then use the VICTORY macro

int main(){

    char myString[BUFFER_SIZE]; // = "Tacos";
    printf("Please enter the secret word: \n");
    scanf("%s", &myString);
    char taco[BUFFER_SIZE] = "Tacos";
    if(strcmp(myString,taco)==0)
    {
        VICTORY();
    }
    else
    {
        printf("You entered:  %s\n", myString);
        printf("The desired secret word is:  %s\n", taco);
    }

    return 0;
}
