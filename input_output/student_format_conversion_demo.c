#include <stdio.h>

int main()
{
// int theAnswer = 42;                      // Life, universe, and everything
// float pi = 3.141592;                     // Pi
// double posSqrtTwo = 1.41421356237;       // Pos square root of 2
// char questionMark = 63;                  // A question mark
// char *nickName[] = {"Mando\0"};      // A nickname

// //////// PRINTF STATEMENTS ////////
// printf("The answer is %d. \n", theAnswer);
// printf("Pi is approx equal to %f. \n", pi);
// printf("The positive square root of 2 is %f. \n", posSqrtTwo);
// printf("Who is %s%c \n", *nickName, questionMark);
// printf("%s is. \n", *nickName);



//another example
// //////// INITIALIZATION ////////
// int num = 3;

// //////// PRINTF STATEMENT ////////
// printf("The square of %d is %d. \n", num, num * num);

// NOTE: Ensure the expressions do not exceed the conversion characters
// If there are too many expressions, the extra ones will be ignored. If there are not enough expressions, 
// C will generate strange numbers for the missing expressions.


/* format specifier precision */

// float someNum = 12.3456;
// printf("\n%f \n", someNum);                      
// printf("%.0f \n", someNum);                    
// printf("%.2f \n", someNum);                    
// printf("%.3f \n", someNum);                    
// printf("%.6f \n", someNum);                   
// printf("%.20f \n", someNum);                  



/* precision strings  */

// char myStr[] = {"Hello world!"};            // Nul terminated string

// printf("\n%s \n", myStr);                        // Hello world!
// printf("%.12s \n", myStr);                     // Hello world!
// printf("%.11s \n", myStr);                     // Hello world
// printf("%.5s \n", myStr);                      // Hello
// printf("%.0s \n", myStr);                      //
// printf("%.1000s \n", myStr);                   // Hello world!

// // NOTE: This "myStr" is dimension 13 and is nul-terminated


/* field width specifier for numbers */

// float someNum = 12.3456;                    // Var used as comparison

// printf("%f this is just f\n", someNum);                     // 12.345600  
// printf("%0f this 0f \n", someNum);                    // 12.345600  
// printf("%2f this 2f \n", someNum);                    // 12.345600
// printf("%4f this 4f \n", someNum);                    // 12.345600
// printf("%5f this 5f \n", someNum);    
// printf("%6f this 6f \n", someNum);  
// printf("%7f this 7f \n", someNum);  
// printf("%8f this 8f \n", someNum);  
// printf("%9f this 9f \n", someNum);  
// printf("%10f this 10f \n", someNum);  
// printf("%11f this 11f \n", someNum);  
// printf("%12f this 12f \n", someNum); 
// printf("%13f this 13f \n", someNum);  
// printf("%14f this 14f \n", someNum);  
// printf("%15f this 15f \n", someNum);                   // 12.345600
// printf("%16f this 16f \n", someNum);                  //         12.345600                (8 positions)
// printf("%17f this 17f\n", someNum); 
// printf("%18f this 18f\n", someNum); 
// printf("%19f this 19f\n", someNum); 
// printf("%20f this 20f\n", someNum); 
// printf("%21f this 21f\n", someNum); 
// printf("%22f this 22f\n", someNum); 
// printf("%23f this 23f\n", someNum); 
// printf("%24f this 24f\n", someNum);               
// printf("%25f this 25f\n", someNum);                   //                  12.3456 ()



/* field specfier to the right */

// printf("%-fthis\n", someNum);                      
// printf("%-0fthis\n", someNum);                    
// printf("%-2fthis\n", someNum);                    
// printf("%-4fthis \n", someNum);                    
// printf("%-8fthis \n", someNum);
// printf("%-9fthis \n", someNum);
// printf("%-10fthis \n", someNum);
// printf("%-11fthis \n", someNum);
// printf("%-12fthis \n", someNum);
// printf("%-13fthis \n", someNum);
// printf("%-14fthis \n", someNum);
// printf("%-15fthis \n", someNum);
// printf("%-16fthis \n", someNum);               
// printf("%-17fthis \n", someNum); 
// printf("%-18fthis \n", someNum); 
// printf("%-19fthis \n", someNum); 
// printf("%-20fthis \n", someNum); 
// printf("%-21fthis \n", someNum); 
// printf("%-22fthis \n", someNum); 
// printf("%-23fthis \n", someNum); 
// printf("%-24fthis \n", someNum);               
// printf("%-25fthis \n", someNum);                  

/* Field width strings */

// char myStr[] = {"Hello world!"};            // Null terminated string

// printf("this is just s %s \n", myStr);                    
// printf("this is 0s %0s \n", myStr);                      
// printf("this is 2s %2s \n", myStr);                       
// printf("this is 4s %4s \n", myStr);                       
// printf("this is 8s %8s \n", myStr);   
// printf("this is 9s %9s \n", myStr);
// printf("this   10s %10s \n", myStr);
// printf("this   11s %11s \n", myStr);
// printf("this   12s %12s \n", myStr);
// printf("this   13s %13s \n", myStr);
// printf("this   14s %14s \n", myStr);
// printf("this   15s %15s \n", myStr);                   
// printf("this   16s %16s \n", myStr);                      
// printf("this   26s %26s \n", myStr);                     

// int len = strlen(myStr);
// printf("\n the length of mystr is: %d \n", len);

/* FLAGS 

%[flags][field width][.precision]specifier

Flags consist of one or more of the following characters:

+
Prefixed to positive numbers

' ' (space)
Prefixed to positive numbers

_
Output is left-justified within field width

0
Field is filled with leading zeros

#
Alternate conversion rules
 */


// float someNum = 12.3456;                    // Var used as comparison

// printf("%f \n", someNum);                     // 12.345600
// printf("%f \n", someNum * -1.0);              // -12.345600
// printf("%+f \n", someNum);                    // +12.345600
// printf("%+f \n", someNum * -1.0);             // -12.3456000
// printf("% f \n", someNum);                    //  12.345600
// printf("% f \n", someNum * -1.0);             // -12.345600    
// printf("%-f \n", someNum);                    // 12.345600  
// printf("%-f \n", someNum * -1.0);             // -12.345600  
// printf("%0f \n", someNum);                    // 12.345600  
// printf("%016f \n", someNum);                  // 000000012.345600
// printf("%.16f \n", someNum);                  // 000000012.345600
/* What is the output? */

// printf("%f \n", 1.2);
// printf("%+8.4f \n", -1.798);
// printf("% 7.2f \n", 0.987654321);
// printf("%-6.1f is yours \n", 13.37);
// printf("Yours is %05.2f \n", 1.2345);
// printf("%s \n", "Hello World!\0");
// printf("%9.5s \n", "Hello world!\0");
// printf("%016.11s \n", "Hello world!\0");


    return 0;
}