#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    char name[100];
    char *power;

    strcpy(name, "Thor");

    // Allocate Dynamic Memory
    power = malloc(200 * sizeof(char));

    // Check if the Pointer is NULL
    if (power == NULL)
    {
        printf ("Error :: Memory not properly allocated.\n");
        return 1;
    }
    strcpy(power, "Lightning Power!!! (for the Thunder God)\n");

    printf("Name : %s\n", name);
    printf("Powers :\n\t%s",power);

    // Discover more power that Thor posseses
    printf("\n========== POWER UPDATE ==========\n\n");
    power = realloc(power, 100*sizeof(*power));

    if (power == NULL)
    {
        printf ("Error :: Memory not properly allocated.\n");
        return 1;
    }
    strcat(power, "\tHAMMER Strength!!!\n");

    printf("Name : %s\n", name);
    printf("Powers :\n\t%s",power);

    free(power);
    power = NULL;
}