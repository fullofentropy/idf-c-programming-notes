/* Definitions

Operand
    Values, variables or expressions which are used to evaluate an expression
         (e.g., 3.14, studentNumber, i)

Operator
    A symbol which operates on a value or variable 
        (e.g., +, =, >, |, &&)

Expression
    A combination of operators and operands 
        (e.g., b+c, i++, !pointer)


*********************************************************************
expression operands operators practice:
Expression:                       Operands:                Operators:

numStu*numClass                 numStu, numClass              *
stuGPA >= avgGPA                stuGPA, avgGPA                >= 
a = b + c                         a, b , c                     =, +
reaminder = (num %2) + 1       reaminder, num, 2, 1      =, %, +
(4*a*c+b*b)/(2*a)              (4*a*c+b*b)  (2*a)          (*, *, + , *, /, *)
********************************************************************                             



*********************Arithmetic Operators************************************

For Mathmatical calculations

Operator	Meaning	        Example	  Result
______________________________________________________
*	       Multiplication	x * y	  The product of x and y
/	       Division	        x / y	  The quotient of x by y
%	       Modulo Division	x % y	  The remainder of x / y
+	       Addition	        x + y	  The sum of x and y
-	       Subtraction	    x - y	  The difference of x and y
+	       Positive sign	+x	      The value of x
-	       Negative sign	-x	      The arithmetic negation of x
++     	   Increment	    ++x	      x = x + 1 (before evaluation)
++	       Increment	    x++	      x = x + 1 (after evaluation)
--	       Decrement	    --x	      x = x - 1 (before evaluation)
--	       Decrement	    x--	     x = x - 1 (after evaluation)




Examples:



Operator	Example	  Result
*	        5 * 3	  15
/	        5 / 3	  1*
%	        5 % 3	  2*
+	        5 + 3	  8
-	        5 - 3	  2
+	        +5	      5
-	        -5	     -5
++	        ++5 	  6*
++	        3++	     3 (4)*
--	        --3	      2*
--	        5--	     5 (4)*

Division Example:
int x = 5; int y = 3;                
////////// EXPRESS AS INTEGER //////////
5 / 3                // Result: 1
5.0 / 3              // Result: 1.666667
5 / 3.0              // Result: 1.666667

////////// TYPE CAST AS FLOAT //////////
(float)x / y         // Result: 1.666667
x / (float)y         // Result: 1.666667
(float)x / (float)y  // Result: 1.666667

 * * * * * * * * * * * * * * * * * * * * 
Modulo example:  Can only be applied to integers
//////// MODULO EXAMPLES ////////
5 % 3                // Result: 2
27 % 16              // Result: 11
30 % 3               // Result: 0
4 % 7                // Result: 4


 * * * * * * * * * * * * * * * * * * * * 
Increment/Decrement:
int x = 5; int y = 3;

//////// INCREMENT ////////
x++                          // Result: 6; x = 5 + 1
++y                          // y = 3 + 1; Result: 4
++x                          // x = 6 + 1; Result: 7
x = 5; y = 3;                // Reset variables

//////// DECREMENT ////////
--y                          // y = 3 - 1; Result: 2
--x                          // x = 5 - 1; Result: 4
y--                          // Result: 1; y = 2 - 1
_______________________________________________________________________________________________________

********************* Relational Operators ************************************

Relational operators check the status of one operand in relation to another operand.

    Every comparison is an expression of data type int
    Comparisons yield the value of 1 or 0
    1 means "true"
    0 means "false"
    Comparisons use relational operators
    Frequently used to test for error conditions



Operator	Meaning	           Example	  Result: 1(true) or 0 (false)
_______________________________________________________________________________
  <	       Less than	        x < y	  1 if x is less than y
  <=	 Less than or equal to	x <= y	  1 if x is less than or equal to y
  >	       Greater than	        x > y	  1 if x is greater than y
  >=  Greater than or equal to	x >= y	  1 if x is greater than or equal to y
  ==	     Equal to	        x == y	  1 if x is equal to y
  !=	    Not equal to	    x != y	  1 if x is not equal to y


Examples:
Operator  Example	Result
  <	       5 < 3	  0
  <=	   5 <= 3	  0
  >	       5 > 3	  1
  >=	   5 >= 3	  1
  ==	   5 == 3	  0
  !=	   5 != 3	  1

NOTE: The equality operators (== and !=) can be used to compare complex numbers. 
Do not, however, use them to compare floats. Floating point numbers end up being 
slightly imprecise due to rounding errors.



 */




