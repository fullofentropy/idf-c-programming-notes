#include <stdio.h> 

/* data types in action */

// include is a directive that tells the preprocessor to insert the contents of another file into the source code at 
// the point where the include directive is found

// functions, such as 'main', are enclosed in braces
// int main(void)
// {
//      //note that the statements end with a semi colon
//      //declare and initialize the variables

//     *** INSERT HERE ***;
   
//     *** INSERT HERE ***;

//     *** INSERT HERE ***;
    
//     char singleChar = *** INSERT HERE ***;

//     char singleChar2 = *** INSERT HERE ***;  //notice this number is being defined as a character and not an integer
   
//     //print statements with print format specifiers
//     //notice how %d is used to specify how we want to format the variable integer
//     printf("Your integer is %d \n", integer);

//     //notice how %f is used to specify how we want to format the variable singlePrecision
//     printf("Your float is %f \n", singlePrecision);

//     //notice how %lf is used to specify how we want to format the variable doublePrecision
//     printf("Your double is %lf \n", doublePrecision);

//     //notice how %c is used to specify how we want to format the variable singleChar
//     printf("Your first char is %c \n", singleChar);

//     //notice how %f is used to specify how we want to format the variable singleChar2 despite it being initalized without quotes
//     printf("Your second char is %c \n", singleChar2);

// }//end main


    
/* example of declaration and initialization */


// int main(void)
// {
// 	/******** VARIABLE INITIALIZATION ********/
//     	/* initialization syntax...
// 	DATA_TYPE VARIABLE_NAME = INITAL_VALUE; */
 
// 	int finalResult;//declaration WITHOUt initilzation

//     //declaration WITH initilization
// 	int lowerLimit = -10;    // lowest y value for a graph
// 	int upperLimit = 10;     // highest y value for a graph
// 	float x = 1.0, y = 2.1;  // graphing variables, multiple declarations
 
// 	// used to hold a person's individual initials
// 	char firstInit = 'C', middleInit = 'M', lastInit = ' ';

//     //error on lastInit because it returns an integer
//     //char firstInit = 'C', middleInit = 'M', lastInit = '';
 
// 	double pi = 3.14159265359;    // stores the data for "pi"
// 	finalResult = 0;    // placeholder 0. to be changed late, being initiliazed after previous declaration.
 
// 	return finalResult;  //commenting a return rresult
// }//end main



/*SIZEOF()*/

// int main(void)
// {
// 	int integer = 1;
// 	float singlePrecision = 2.2;
// 	double doublePrecision = 3.3;
// 	char singleChar1 = '$';
// 	char singleChar2 = 33;
 
// 	printf("size of int is %d bytes \n", *** INSERT HERE ***);
// 	printf("size of float is %d bytes \n", *** INSERT HERE ***);
// 	printf("size of double is %d bytes \n", *** INSERT HERE ***);
// 	printf("size of char 1 is %d bytes \n", *** INSERT HERE ***);
// 	printf("size of char 2 is %d bytes \n", *** INSERT HERE ***);
 
// 	return 0;
// }//end main