/*  C Predefined Data Types 
*

Objects, functions, and expressions have a property called type.
*** INSERT HERE ***

A variable's type regulates:

- *** INSERT HERE ***
- *** INSERT HERE ***
- *** INSERT HERE ***

C - "types" can be either predefined or derived.

**********************************

Derived Data types/Brief:
    *** INSERT HERE ***
    *** INSERT HERE ***


Note - Floating point definition
    Floating point is a technique that allows the point to float around, 
    allowing you to express very large numbers or very small ones,
    without giving either the fractional or the whole a set number of digits.
    To express a large number, the floating point moves all the way to the right, 
    and the opposite to express a small number.

Predefined Types/More in depth: 

 
    void -  *** INSERT HERE ***

    int - A*** INSERT HERE ***
        *** INSERT HERE ***
        *** INSERT HERE ***: [-32,768, 32,767]  (16 bits)
       *** INSERT HERE ***: [-2,147,483,648, 2,147,483,647]  (32 bits)
    
    char - Stores a single character/one character in a local character set
           *** INSERT HERE ***
           Can be signed or unsigned
           *** INSERT HERE ***
    
    float - Single precision decimal number (32 bits are used to represent floating point number)
                     *** INSERT HERE ***
                     Range of 1.2 X 10^-38 to 3.4 X 10^+38
                    *** INSERT HERE *** 

    double - Double-precision floating point.
             *** INSERT HERE ***
            *** INSERT HERE ***

More on single and double precision definitons:
https://www.geeksforgeeks.org/difference-between-single-precision-and-double-precision/
    

***********************************************************

Keywords

Keywords are reserved words used in a programming language. 
They cannot be used as a programmer-defined identifier or as names for variables.
Each word has fixed meaning, this meaning cannot be changed by a user.


auto
break
case
char
const
continue
default
do
double
else
enum
extern
float
for
goto
if
inline (since C99)
int
long
register
restrict (since C99)
return
short
signed
sizeof
static
struct
switch
typedef
union
unsigned
void
volatile
while

_Alignas (since C11)
_Alignof (since C11)
_Atomic (since C11)
_Bool (since C99)
_Complex (since C99)
_Decimal128 (since C23)
_Decimal32 (since C23)
_Decimal64 (since C23)
_Generic (since C11)
_Imaginary (since C99)
_Noreturn (since C11)
_Static_assert (since C11)
_Thread_local (since C11)

    The most common keywords that begin with an underscore are generally used through their convenience macros:

    keyword	used as	defined in
    _Alignas (since C11)	alignas	stdalign.h
    _Alignof (since C11)	alignof	stdalign.h
    _Atomic (since C11)	atomic_bool, atomic_int, ...	stdatomic.h
    _Bool (since C99)	bool	stdbool.h
    _Complex (since C99)	complex	complex.h
    _Decimal128 (since C23)	(no macro)	
    _Decimal32 (since C23)	(no macro)	
    _Decimal64 (since C23)	(no macro)	
    _Generic (since C11)	(no macro)	
    _Imaginary (since C99)	imaginary	complex.h
    _Noreturn (since C11)	noreturn	stdnoreturn.h
    _Static_assert (since C11)	static_assert	assert.h
    _Thread_local (since C11)	thread_local	threads.h

For C keyword refrence:
https://en.cppreference.com/w/c/keyword


Organizations that standardize C:
    American National Standards Institute (ANSI)-C
    Any version of C that conforms to the specifications of the American National Standards Institute committee X3J

    International Organization for Standardization (ISO)
    ISO is an international standard-setting organization, 
    composed of representatives from various national standards organizations.



C history:
American National Standards Institute (ANSI)-C
The first standard for C was published by ANSI

Dennis Ritchie started developing the C programming language (Fig. 1) in 1969 at AT&T’s Bell Labs. 
In 1972, Dennis Ritchie and Brian Kernighan delivered the quintessential book, The C Programming Language, that defined C.

C was used as the system programming language for AT&T’s UNIX and eventually Linux as well as a host of other operating systems.
In 1983, the American National Standards Institute (ANSI) formed the X3J11 committee that developed the first ANSI C known as C89. 
It was essentially based on the standard UNIX C compiler of the time.

In 1990, International Organization for Standardization (ISO) created ISO/IEC 9899:1990, or C90, that matched ANSI’s C89. 
Most C compilers support C89/C90 at a minimum with extensions or support for other C standards like C99 as well. 
The ISO/IEC JTC1/SC22/WG14 working group now develops the C standards.
C18
The current standard is ISO/IEC 9899:2018 (aka C17 and C18) 


*********************************************************************************************************
Declaration  _  Initialization
______________________________

Declaratiation:
    All variable must be declared before use!

   *** INSERT HERE ***
    Identifiers are the name assigned to an element in a program ie, variable name, function name

   *** INSERT HERE ***

    Multiple variables of the same data type can be declared in a single statement.

    example: int x;
            float y, z;

Initialization: 
    Must be done after you declare a variable!

    *** INSERT HERE ***

    *** INSERT HERE ***

    initialization syntax...
	DATA_TYPE VARIABLE_NAME = INITAL_VALUE;

    example:
        int i = 1;
        float y, z = 2.2;

*****************************************************************************
SIZEOF()
______

sizeof() is used to determine the amount of memory taken up. Never guess; let the system tell us.

Size sets limits on the information that can be stored.
*** INSERT HERE ***
NEVER ASSUME DATA TYPE SIZE

You allocate or de-allocate memory. You manage buffers and thus know the performance impact of your actions.

Operator sizeof():
    used to determine the amount of memory taken up
    example:
        int i = 1;
        sizeof(i);



*
*/

