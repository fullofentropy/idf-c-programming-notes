#include <stdio.h>

void add(int a, int b)
{
    printf("Addition is %d\n", a+b);
}
void sub(int a, int b)
{
    printf("Subtraction is %d\n", a-b);
}
void mult(int a, int b)
{
    printf("Multiplication is %d\n", a*b);
}
void div(int a, int b)
{
    if(b == 0)
    {
        printf("Cannot Divide by 0\n");
    }
    else
    {
        printf("Division is %d\n", a/b);
    }
}

int main()
{
    void (*calc_ptr_arr[])(int, int) = {add, sub, mult, div};

    int a;
    int b;
    int fun;

    printf("Enter two Ints.\n");
    scanf("%d %d", &a, &b);
    printf("\nEnter Choice:\n0 for Addition\n1 for Subtraction\n2 For Multiplication\n3 For Division\n");
    scanf("%d", &fun);

    if (fun > 3){
        printf("Please Enter a Valid Choice\n");
        return 0;
    }
    (*calc_ptr_arr[fun])(a, b);
    return 0;
}