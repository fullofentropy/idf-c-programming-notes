#include <stdio.h>
/* type conversion:

    implicit: performed by the compiler automatically

    explicit: through the use of the cast operator
    Best practice is to use the cast operator when type conversion is necessary.

    // pseudo example
    (data_type) expression // not everything can be type cast


     */
	
int main(void)
{   

    // /* Cast int to double */
    // ??? grade1 = 90, grade2 = 100;    // student grades, declare and intit on one line
    
    // // variable to hold average as a double
    // ??? doublegradeAvg = 0;               // student average
    // //casting the int grades to a double to store in the double variable of gradeAvg
    // doublegradeAvg = (???) ((grade1 + grade2) / 2);

    // printf("With type conversion from into to double \n");
    // printf("The average as a float: %??? \n", doublegradeAvg);

    // printf("The average as a float formatted to two decimal places: %.02f \n", doublegradeAvg);
    
     /**************************** Cast doubles to int **********************************/
    // ??? grade1 = 90, grade2 = 100;    // student grades 
    
    // // variable to hold average as an int
    // ??? intgradeAvg = 0;               // student average

    // intgradeAvg = (???) ((grade1 + grade2) / 2);

    // printf("With type conversion from double to int \n");
    // printf("The average as an int: %??? \n", intgradeAvg);
  
    return 0;
}