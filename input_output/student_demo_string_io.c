#include <stdio.h>

int main()
{   
  /*   fgets() Reads n-1 characters from stream into buffer str */

    // char buff[4];                        // Will store string
    // printf("Enter string: ");            // Prompts user
    // ??????;    // Stores user string
    // printf("Your string was: ");         // Prefaces output
    // /* puts() Writes string str and a trailing \n to stdout */
    // ??????;

    /* try with this input:
    Enter a string: 123
    Enter a string: abcd
    Enter a string: 31398
    Enter a string: Superman */

    return 0;
}

