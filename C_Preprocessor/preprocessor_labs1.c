#include <stdio.h>
#include <string.h>

#define BUFFER_SIZE 64

#define VICTORY() \
    {  \
        printf("You typed the Magic Tacos Passcode\n");  \
        printf("returning zero\n");   \
        return 0;   \
    }

int main()
{
    char myString[BUFFER_SIZE];
    printf("Please enter the secret word: \n");
    scanf("%s", &myString);
    char taco[BUFFER_SIZE] = "Tacos";

    if(strcmp(myString,taco)==0)
    {
        VICTORY();
    }
    else
    {
        printf("You entered:  %s\n", myString);
        printf("The desired secret word is:  %s\n", taco);
    }

    return 0;
}



