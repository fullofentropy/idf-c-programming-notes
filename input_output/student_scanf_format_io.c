/* SCANF


scanf()

int scanf(const char *format, argument-1, argument-2...);

Purpose: Reach characters from stdin and saves converted values into argument-1 ... argument-n

Arguments: Format string, storage locations

Return Value: Number of successful input items, EOF on error or failure

Special:
    Conversion specifications translate input
    Each conversion specifier is preceded by a %
    Specify field width (maximum number of character to be read and converted) or skip input with *
EVIL: scanf() is unsafe! Use caution!
scanf() has an unsafe interface for strings. 
It has no way of knowing that your variable parameter is an array large enough to hold the 
input plus a terminating null character.
 Scanf will continue to read character from stdin and store them in memory well past the end of 
 your char array, causing a buffer overrun vulnerability.



Basic Syntax Examples

//////////////////////////////////
///// BASIC SCANF STATEMENTS /////
//////////////////////////////////

scanf("%d", &theAnswer);            // Reads an integer from stdin
scanf("%f", &pi);                   // Reads a float from stdin
scanf("%lf", &posSqrtTwo);          // Reads a double from stdin
scanf("%c", &answerNum1);           // Reads a char from stdin

 ///// BASIC SCANF STRING INPUT /////
char nickname[20] = {0};
scanf("%s", nickName);             // Dangerously reads string from stdin
scanf("%19s", nickname);           // Safer string read from stdin
NOTE: When using field width on scanf string input, leave room for the nul terminator

 */