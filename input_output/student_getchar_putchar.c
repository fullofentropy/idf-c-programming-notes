/* C Streams Functions

Example Functions:

Some functions take input, aka arguments
    printf()

Some functions do not require arguments
    getchar()

Some functions give output, aka return value
    putchar()

Some functions have no return value
    exit()


Function Syntax:
<return type> <function name> (<arg type> <argument>, ...)

Example 1 - printf()
    int printf(const char *format, ...)

Example 2 - getchar()
    int getchar(void)

Example 3 - putchar()
    int putchar(int c)

Example 4 - exit()
    void exit(int status)


Purpose of i/o functions:


getchar():
    Gets unsigned int from stdin
    Arguments: None
    Return Value: This function returns the character read as an unsigned char cast to an int 
    or 
    EOF on end of file
    or error.
    
    Syntax Example: 

        int userInput = 0;                    // Will store input
        printf("Enter a character: ");        // Prompts user
        userInput = getchar();                // Stores user input into userInput
    
putchar()

    int putchar(int char)
    Purpose: Writes an unsigned char to stdout
    Arguments: Integer value of character to write
    Return Value: This function returns the character read as an unsigned char cast to an int or EOF on end of file or error.
    Syntax Example:

    printf("Your character was" );        // Prefaces output
    putchar(userInput);                   // Prints output


*/