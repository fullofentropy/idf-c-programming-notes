#include <stdio.h>

int main()
{
   /*  fprintf() */
   /* int fprintf(FILE *stream, const char *format, ...) */

//    //////// VARIABLE INITIALIZATION ////////
//     int theAnswer = 42;            // Life, universe, & everything
//     float pi = 3.141592;            // Pi
//     double posSqrtTwo = 1.41421356237;    // Positive square root of 2
//     char questionMark = 63;        // A question mark
//     char *nickName[] = {"Gilligan\0"};    // A nick name
    

//     //////// FPRINTF STATEMENTS /////////////
//     fprintf(stdout, "The answer is %d. \n", theAnswer);
//     fprintf(stdout, "Pi is approximately equal to %f. \n", pi);
//     fprintf(stdout, "The square root of 2 is %f. \n", posSqrtTwo);
//     fprintf(stdout, "Who is %s%c \n", nickName, questionMark);
//     fprintf(stdout, "%s is. \n", nickName);


/* fscanf() */

/* int fscanf(FILE *stream, const char *format, ...) */

    int theAnswer = 42;            // Life, universe, & everything
    float pi = 3.141592;            // Pi
    double posSqrtTwo = 1.41421356237;    // Positive square root of 2
    char answerNum1 = 'a';        // A question mark
    // char *nickName[] = {"Gilligan\0"};    // A nick name

/////////////////////////////////////////
//////// BASIC FSCANF STATEMENTS ////////
/////////////////////////////////////////
// printf("Enter an integer: \n");
// fscanf(stdin, "%d", &theAnswer);    // Reads an integer from stdin

// printf("Enter an float: \n");
// fscanf(stdin, "%f", &pi);        // Reads a float from stdin

// printf("Enter an double: \n");
// fscanf(stdin, "%lf", &posSqrtTwo);    // Reads a double from stdin

// printf("Enter an character: \n");
//note that there is a space before the percent sign for the character formate specifier
// fscanf(stdin, " %c", &answerNum1);     // Reads a character from stdin

// printf("You entered: %d, %f, %lf, %c \n", theAnswer, pi, posSqrtTwo, answerNum1);

/////// BASIC SCANF STRING INPUT ////////
// char nickName[20] = {0};

// printf("Enter an nickname: \n");
// // fscanf(stdin, "%s", nickName);    // Still dangerous
// fscanf(stdin, "%19s", nickName);    // Safer string read from stdin
// printf("%s", nickName);



    return 0;
}