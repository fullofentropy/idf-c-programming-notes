#include <stdio.h>

int main()
{       
    /***************** getchar example ***************************/

    // int userInput = 0;                    // Will store input
    // printf("Enter a character: ");        // Prompts user
    // ?????               // Stores user input into userInput
    // printf("\n You entered: %c", userInput);

    /***************** putchar example ***************************/

    // int userInput = 0;                    // Will store input
    // printf("Enter a character: ");        // Prompts user
    // ??????;                // Stores user input into userInput
    // printf("Your character was :" );        // Prefaces output
    // ??????;                   // Prints output



    return 0;
}