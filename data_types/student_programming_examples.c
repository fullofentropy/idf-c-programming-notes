#include <stdio.h>

int main()
{   

    /********************************** Program to Find the Size of a variable *********************************/
    // int integerType;
    // float floatType;
    // double doubleType;
    // char charType;
    // // Sizeof operator is used to evaluate the size of a variable
    // printf("Size of int: %??? bytes\n",sizeof(???));
    // printf("Size of float: ??? bytes\n",sizeof(???));
    // printf("Size of double: ??? bytes\n",sizeof(???));
    // printf("Size of char: ??? byte\n",sizeof(???));


   /****************************** Program to Print an Integer by getting info from user**************************/
    // int testInteger;
    // printf("Enter an integer: ");
    // scanf("%d", &testInteger);  
    // printf("Number = %d",testInteger);
 
   /*************************************** Program to add two intengers ****************************************/
    // int firstNumber, secondNumber, sumOfTwoNumbers;
    
    // printf("Enter two integers: ");
    // // Two integers entered by user is stored using scanf() function
    // scanf("??? ???", &???, &???);
    // // sum of two numbers in stored in variable sumOfTwoNumbers
    // sumOfTwoNumbers = firstNumber + secondNumber;
    // // Displays sum      
    // printf("??? + ??? = ???", firstNumber, secondNumber, sumOfTwoNumbers);


   /**************************************  Program to multiply two number ********************************************/
    // double firstNumber, secondNumber, product;
    // printf("Enter two numbers: ");
    // // Stores two floating point numbers in variable firstNumber and secondNumber respectively
    // scanf(???);  
 
    // // Performs multiplication and stores the result in variable productOfTwoNumbers
    // product = firstNumber * secondNumber;  
    // // Result up to 2 decimal point is displayed using %.2lf
    // printf(???);

   /*************************************  Program to print ASCII value **************************************************/
    //  char c;
    //  printf("Enter a character: ");
    //  // Reads character input from the user
    //  scanf(???);  
    
    //  // %d displays the integer value of a character
    //  // %c displays the actual character
    //  printf("ASCII value of ??? = ???", ???, ???);

    /********************************  Program to Compute Quotient and Remainder ******************************************/
    // int dividend, divisor, quotient, remainder;
    // printf("Enter dividend: ");
    // scanf(???);
    // printf("Enter divisor: ");
    // scanf(???);
    // // Computes quotient
    // quotient = dividend / divisor;
    // // Computes remainder
    // remainder = dividend % divisor;
    // printf("Quotient = ???\n", ???);
    // printf("Remainder = ???",???);

    /****************************************  Program to swap numbers **************************************************/
    // double firstNumber, secondNumber, temporaryVariable;
    // printf("Enter first number: ");
    // scanf("%lf", &firstNumber);
    // printf("Enter second number: ");
    // scanf(???);
    // // Value of firstNumber is assigned to temporaryVariable
    // temporaryVariable = firstNumber;
    // // Value of secondNumber is assigned to firstNumber
    // firstNumber = secondNumber;
    // // Value of temporaryVariable (which contains the initial value of firstNumber) is assigned to secondNumber
    // secondNumber = temporaryVariable;
    // printf("\nAfter swapping, firstNumber = %.2lf\n", firstNumber);
    // printf("After swapping, secondNumber = ???", secondNumber);

     /*************************  Program to swap numbers Without Using Temporary Variables *****************************/
    // double firstNumber, secondNumber;
    // printf("Enter first number: ");
    // scanf(???);
    // printf("Enter second number: ");
    // scanf(???);
    // // Swapping process
    // firstNumber = firstNumber - secondNumber;
    // secondNumber = firstNumber + secondNumber;
    // firstNumber = secondNumber - firstNumber;
    // //printf formatted to two decimal places
    // printf("\nAfter swapping, firstNumber = ???\n",???);
    // printf("After swapping, secondNumber = ???", ???);


    return 0;
}