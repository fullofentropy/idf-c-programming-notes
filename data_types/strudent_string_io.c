/* String IO

gets()

gets_s()
    DO NOT USE
    gets() function is a common source of buffer overflow vulnerabilities and should never be used.
    gets_s() crashes if buffer is too small and requires careful use

fgets()
    fgets()

    char *fgets(char *str, int n, FILE *stream);

    Purpose: Reads n-1 characters from stream into buffer str

    Arguments: Buffer str, buffer size n, stream pointer

    Return Value: str on sucess,
    NULL on failure

    Syntax Example:

        char buff[256];                    // Will store string
        printf("Enter a string: ");        // Prompts user
        fgets(buff, sizeof(buff), stdin);  // Stores user string
        NOTE: gets_s() should ensure your array is nul-terminated

    Though it is recommended you verify your string is nul-terminated. Don't get complacent


puts()

    int puts(const char *str);

    Purpose: Writes string str and a trailing \n to stdout

    Arguments: Buffer str

    Return Value - Positive # on sucess 
    or EOF on error

    Syntax Example:

        printf("Your string was: ");            // Prefaces output
        puts(buff);                             // Writes to stdout
        NOTE: Ensure C strings are nul-terminated**

fputs()

int fputs(const char *str, FILE *stream);

Purpose: Writes string str to stream

Arguments: Buffer str, output stream

Return Value: Positive # on success 
or EOF on error

Syntax Example:

    printf("Your string was: ");            // Prefaces output
    fputs(buff, stdout);                    // Writes to stdout
    NOTE: Ensure C strings are nul-terminated


 */