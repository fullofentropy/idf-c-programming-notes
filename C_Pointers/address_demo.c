#include <stdio.h>

int main(void) {
  int *gerald;
  int doug;

  doug = 22;
  printf("Doug's address is : %p\n", &doug);
  printf("Doug's age is : %d\n", doug);
  
  gerald = &doug;
  printf("Gerald is pointing to : %p\n", gerald);
  printf("Gerald's address is : %p\n", &gerald);
  printf("Doug's age in Geralds eyes %d\n", *gerald);

  doug = 28;
  printf("Address of doug %p\n", gerald);
  printf("Dougs value is : %d\n", *gerald);

  *gerald = 24;
  printf("Dougs value is now : %d\n", doug);
  printf("Dougs address is now %p\n", &doug);

  return 0;
}